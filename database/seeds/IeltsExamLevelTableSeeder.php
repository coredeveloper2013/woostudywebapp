<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class IeltsExamLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        App\IELTSExamLevel::insert(
            [
                [
                    'title' => 'IELTS',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'created_at' => $faker->dateTimeThisMonth,
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth
                ],
                [
                    'title' => 'TOFEL',
                    'status' => $faker->boolean,
                    'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'created_at' => $faker->dateTimeThisMonth,
                    'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
                    'approved_at' => $faker->dateTimeThisMonth
                ]
            ]
        );
    }
}
