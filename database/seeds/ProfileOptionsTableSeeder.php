<?php

use App\Libraries\Helpers;
use App\ProfileOption;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProfileOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      ProfileOption::truncate();
      $faker = Faker::create();
      foreach(range(1, 4) as $i) {
        $fields = [
          'date_of_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
          'gender' => 'Male',
          'country' => $faker->country,
          'city' => $faker->city,
          'address' => $faker->address,
          'facebook_url' => 'https://facebook.com',
          'twitter_url' =>  'https://twitter.com',
          'instragram_url' =>  'https://instragram.com',
          'google_url' =>  'https://google.com',
        ];
        Helpers::set_or_update_profile_options_by_fields($fields, $i);
      }

    }
}
