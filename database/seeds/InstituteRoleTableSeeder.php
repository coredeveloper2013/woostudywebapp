<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class InstituteRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        App\InstituteRole::insert(
            [
                [
                    'title' => 'Administration',
                    'slug' => 'administration',
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],
                [
                    'title' =>'Marketing',
                    'slug' => 'marketing',
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],
                [
                    'title' =>  'Lecturer',
                    'slug' => 'lecturer',
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ],
                [
                    'title' => 'Department Head',
                    'slug' => 'department-head',
                    'created_at' => $faker->dateTimeThisMonth,
                    'updated_at' => $faker->dateTimeThisMonth
                ]
            ]
        );
    }
}
