<?php

use App\Libraries\Helpers;
use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::truncate();
      foreach (Helpers::ROLES as $role) {
        Role::create([
          'title' => $role['name']
        ]);
      }
    }
}
