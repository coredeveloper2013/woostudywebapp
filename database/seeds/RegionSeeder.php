<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Region::insert(
            [
                [
                    'title' => 'Africa',
                    'slug' => 'africa'
                ],
                [
                    'title' => 'Americas',
                    'slug' => 'americas'
                ],
                [
                    'title' => 'Asia',
                    'slug' => 'asia'
                ],
                [
                    'title' => 'Europe',
                    'slug' => 'europe'
                ],
                [
                    'title' => 'Oceania',
                    'slug' => 'oceania'
                ]
            ] );
    }
}
