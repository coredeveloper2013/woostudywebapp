<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'educations', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'user_id' );
            $table->unsignedInteger( 'institute_id' )->nullable();
            $table->unsignedInteger( 'education_level_id' )->nullable();
            $table->string( 'title' );
            $table->string( 'grade' )->nullable();
            $table->date( 'completion_date' )->nullable();
            $table->integer( 'percentage' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'educations' );
    }
}
