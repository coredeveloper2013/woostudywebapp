<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstituteAuthorUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'institute_author_user', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'institute_id' );
            $table->unsignedInteger( 'department_id' );
            $table->unsignedInteger( 'institute_role_id' );
            $table->unsignedInteger( 'created_by' );
            $table->string( 'name' );
            $table->string( 'email' );
            $table->string( 'phone' )->nullable();
            $table->string( 'mobile' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'institute_author_user' );
    }
}
