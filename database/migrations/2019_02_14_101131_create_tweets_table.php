<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {

            $table->increments('id');
            $table->text('tweet');
            $table->string('category')->nullable();

            $table->text('entities');
            $table->enum('user_sentiment', ['Positive','Negative','Neutral'])->nullable();

            $table->string('predictied_sentiment')->nullable();
            $table->text('classifications')->nullable();
            $table->text('concepts')->nullable();

            $table->string('search_query')->nullable();
            $table->enum('is_enriched', ['Yes', 'No'])->default('No');
            $table->string('tweet_id')->unique();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tweets');
    }
}
