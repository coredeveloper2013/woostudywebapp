<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'comments', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'post_id' );
            $table->unsignedInteger( 'user_id' );
            $table->tinyInteger( 'status' )->default( true );
            $table->text( 'body' );
            $table->timestamps();
            $table->foreign( 'post_id' )->references( 'id' )->on( 'posts' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'comments' );
    }
}
