<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'courses', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'education_level_id' );
            $table->unsignedInteger( 'user_id' );
            $table->string( 'title' );
            $table->string( 'fee' )->nullable();;
            $table->date( 'starting_date' )->nullable();;
            $table->text( 'description' )->nullable();
            $table->boolean( 'licence_required' )->default( false );
            $table->string( 'licence_id' )->nullable();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'courses' );
    }
}
