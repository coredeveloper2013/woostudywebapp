<?php

use Faker\Generator as Faker;

$factory->define( \App\Institute::class, function (Faker $faker) {
    return [

        'title' => ucfirst( $faker->word ) . ' Institute',
        'education_level_id' => $faker->randomElement( \App\EducationLevel::pluck( 'id' )->toArray() ),
        'user_id' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'created_at' => $faker->dateTimeThisMonth,
        'updated_at' => $faker->dateTimeThisMonth
    ];
} );
