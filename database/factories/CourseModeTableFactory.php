<?php

use Faker\Generator as Faker;

$factory->define( \App\CourseServiceMode::class, function (Faker $faker) {
    return [
        'mode' => $faker->randomElement( ['home', 'tuition', 'remote'] ),
        'course_id' => $faker->randomElement( \App\Course::pluck( 'id' )->toArray() ),
        'created_at' => $faker->dateTimeThisMonth,
        'updated_at' => $faker->dateTimeThisMonth
    ];
} );
