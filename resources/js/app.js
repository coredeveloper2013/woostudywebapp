/**
 * app.js will have only axios and loadash
 */

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.$ = window.jQuery = require('jquery');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import router from './routes';
import index from './store';
import filters from './filters';
import 'lodash';
import Vue from "vue"
import VeeValidate from 'vee-validate';
import VueMoment from 'vue-moment';
import VModal from 'vue-js-modal';
import VueSweetalert2 from 'vue-sweetalert2';
import BootstrapVue from 'bootstrap-vue'



[VModal, filters, VeeValidate, VueMoment, VueSweetalert2, BootstrapVue].forEach((x) => Vue.use(x));

Vue.component('homepage', require('./pages/common/HomePage'));

const app = new Vue({
    el: '#app',
    router,
    store: index
});
