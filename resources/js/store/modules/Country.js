import api from "../../config/api";

const state = {
    countries: []
};

const getters = {
    countries(state) {
        return state.countries;
    },
    countriesByRegion(state) {
        return id => state.countries.filter(value => {
            return value.region_id === id
        });
    }
};

const mutations = {
    countryListMutation(state, payleoad) {
        return state.countries = payleoad;
    }
};

const actions = {
    async countryListAction({commit}) {
        commit('countryListMutation', await axios.get(api.countries).then(response => {
            return response.data;
        }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}