import api from "../../config/api";

const state = {
    notifications: [],
    count: 0
};

const getters = {
    notification(state) {
        return state.notifications;
    },
    count(state) {
        return state.count;
    }
};

const mutations = {
    mutationNotification(state, payleoad) {
        return state.notifications = payleoad;
    },
    mutationNotificationCount(state, payleoad) {
        return state.count = payleoad;
    },
    mutationUpdateNotificationCount(state, payleoad) {
        return state.count = payleoad;
    }
};

const actions = {
    async actionNotification({commit}) {
        commit('mutationNotification', await axios.get(api.notificationList)
            .then(response => {
                return response.data;
            })
        )
    },
    async actionNotificationCount({commit}) {
        commit('mutationNotificationCount', await axios.get(api.notificationCount)
            .then(response => {
                return response.data;
            })
        );
    },
    actionUpdateNotificationCount(context, payLoad) {
        context.commit('mutationUpdateNotificationCount', payLoad);
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}