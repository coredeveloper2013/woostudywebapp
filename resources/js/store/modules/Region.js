import api from "../../config/api";

const state = {
    regions: {}
};

const getters = {
    regions(state) {
        return state.regions;
    }
};

const mutations = {
    regionListMutation(state, payleoad) {
        return state.regions = payleoad;
    }
};

const actions = {
    async regionListAction({commit}) {
        commit('regionListMutation', await axios.get(api.regions).then(response => {
            return response.data;
        }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}