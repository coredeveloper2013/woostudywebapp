import api from "../../config/api";
import router from '../../routes'

const state = {
    roles: ['admin', 'students', 'tutor'],
    authUser: {},
    profile_id: null,
};

const getters = {
    roles(state) {
        return state.roles;
    },
    authUser(state) {
        return state.authUser;
    },
    avater(state) {
        return state.authUser.avater ? state.authUser.avater : 'author.jpg';
    },
    profile_id(state) {
        return state.profile_id;
    },

    roleId(state) {
        return state.authUser.role_id;
    },
    auth_user_id(state) {
        return state.authUser.id;
    },
    institute_author(state) {
        if (state.authUser.role_id === 3) {
            return state.authUser.institute_author
        }
    }

};

const mutations = {
    addRolesMutation(state, payleoad) {
        return state.roles = [...state.roles, payleoad]
    },
    setAuthUserDataMutation(state, payleoad) {
        if (payleoad && payleoad.role_id === 3 && !payleoad.institute_author) {
            router.push({name: 'Institute Author User Details'})
        }
        return state.authUser = payleoad
    },
    setProfileIdMutation(state, payleoad) {
        return state.profile_id = payleoad;
    },
    updateInstituteAuthorUserMutation(state, payleoad) {
        state.authUser.institute_author = payleoad
    },
    setUserAvater(state, payleoad) {
        state.authUser.avater = payleoad
    }
};

const actions = {
    addRolesAction(context, payLoad) {
        context.commit('addRolesMutation', payLoad);
    },
    async setAuthUserDataAction({commit}) {
        commit('setAuthUserDataMutation', await
            axios.get(api.authUser).then(response => {
                return response.data;
            }))
    },
    setProfileIdAction(context, payLoad) {
        context.commit('setProfileIdMutation', payLoad);
    },
    updateInstituteAuthorUserAction(context, payLoad) {
        context.commit('updateInstituteAuthorUserMutation', payLoad);
    },
    setUserAvater(context, payLoad) {
        context.commit('setUserAvater', payLoad);
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}