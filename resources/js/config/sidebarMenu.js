const sidebarMenu =
    [
        {
            groups: "Profile",
            role_id: ['*'],
            route_name: 'profile',
            className: 'fa fa-fw fa-user fa-lg',
            menus: [
                {
                    title: "Basic Information",
                    role_id: [1, 2, 4, 5],
                    route_name: 'User Personal  Info',
                },
                {
                    title: "Basic Information",
                    role_id: [3],
                    route_name: 'Institute Basic Information',
                },
                {
                    title: "Contact Us",
                    role_id: [1, 2, 4, 5],
                    route_name: 'User Connect',
                },
                {
                    title: "Contact Details",
                    role_id: [3],
                    route_name: 'Institute Branches',
                },
                {
                    title: "Education",
                    role_id: [2, 4],
                    route_name: 'User Education',
                },
                {
                    title: "Ielts/Tofel Scores",
                    role_id: [2],
                    route_name: 'User Ielts Tofel',
                },
                {
                    title: "Interests",
                    role_id: [2],
                    route_name: 'User Interest',
                },
                {
                    title: "License",
                    role_id: [4],
                    route_name: 'User Licence',
                },
                {
                    title: "Work Experience",
                    role_id: [4],
                    route_name: 'User Experience',
                },
                {
                    title: "Videos",
                    role_id: ['*'],
                    route_name: 'User Video Block',
                },
            ],
        },
        {
            groups: "Upgrade to premium",
            role_id: ['*'],
            className: 'fa fa-address-book  fa-lg',
            route_name: 'User Connect',
        },
        {
            groups: "Offer Courses",
            role_id: [4],
            className: 'fa fa-graduation-cap fa-lg',
            route_name: 'Offer Courses',
        },
        {
            groups: "Connections",
            role_id: ['*'],
            className: 'fa fa-address-book  fa-lg',
            route_name: 'User Connect',
            menus: [
                /*{
                    title: "Find Mutual Friend",
                    role_id: [2,3,4],
                    route_name: 'Find Mutual Friend',
                },*/

                /*Student Role*/
                {
                    title: "Find Institute",
                    role_id: [2],
                    route_name: 'Find Institute',
                }, {
                    title: "Find Tutors",
                    role_id: [2],
                    route_name: 'Find Tutors',
                },
                {
                    title: "Find Others Student",
                    role_id: [2],
                    route_name: 'Find Student',
                },

                /*School Role*/
                {
                    title: "Find Student",
                    role_id: [3],
                    route_name: 'Find Student',
                }, {
                    title: "Find Tutors",
                    role_id: [3],
                    route_name: 'Find Tutors',
                },
                {
                    title: "Find Others Schools",
                    role_id: [3],
                    route_name: 'Find Institute',
                },


                /*Teacher Role*/

                {
                    title: "Find Others Teachers",
                    role_id: [4],
                    route_name: 'Find Tutors',
                },
                {
                    title: "Find Student",
                    role_id: [4],
                    route_name: 'Find Student',
                },
                {
                    title: "Find others Schools",
                    role_id: [4],
                    route_name: 'Find Institute',
                },

                /*All Role*/
                {
                    title: "Explore Social Media",
                    role_id: [2, 4],
                    route_name: 'Social Media Link',
                },
                {
                    title: "Invitations",
                    role_id: ['*'],
                    route_name: 'User Invitations List',
                }

            ]
        },
        {
            groups: "Chat",
            role_id: ['*'],
            className: 'fa fa-comment fa-lg',
            route_name: 'Chatting Page',
        },
        {
            groups: "Social Media",
            role_id: [3],
            className: 'fa fa-share-square-o fa-lg',
            route_name: 'Social Media Dashboard',
        },
        {
            groups: "Notifications",
            role_id: ['*'],
            className: 'fas fa-bell',
            route_name: 'User Notification Page',
        },
        {
            groups: "Education Programs",
            role_id: [3],
            className: 'fa fa-graduation-cap fa-lg',
            route_name: 'Education programs',
            menus: [
                {
                    title: "Offer a programs",
                    role_id: [3],
                    route_name: 'Offer a programs',
                },
            ]
        },
        {
            groups: "Login and Security",
            role_id: ['*'],
            className: 'fa fa-lock',
            route_name: 'Login and Security',
            menus: [
                {
                    title: "Change Password",
                    role_id: ['*'],
                    route_name: 'Change Password',
                },
            ]
        }
    ];
export default sidebarMenu;