import Slugify from './Slugify';
import strLimit from './StrLimit';

export default {
    install(Vue) {
        Vue.filter('slugify', Slugify);
        Vue.filter('strLimit', strLimit);
    }
}