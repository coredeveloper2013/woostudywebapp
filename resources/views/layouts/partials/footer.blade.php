<footer class="footer_area">
    <div class="footer_newsletter">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="footer_newsletter_inner">
                        <h2>Subscribe to our Newsletter</h2>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="newsletter_form">
                        <input type="text" class="form-control" placeholder="Enter your email address">
                        <button>subscribe</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom_area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="footer_menu_inner">
                        <ul>
                            <li><a href="#">How it Works</a></li>
                            <li><a href="#">Appraoch</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Blogs</a></li>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="footer_menu_inner">
                        <h2>Students:</h2>
                        <ul>
                            <li><a href="#">News and Events</a></li>
                            <li><a href="#">Support Unit</a></li>
                            <li><a href="#">Safety</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="footer_menu_inner">
                        <h2>Academic:</h2>
                        <ul>
                            <li><a href="#">How it Works</a></li>
                            <li><a href="#">Support Unit</a></li>
                            <li><a href="#">Safety</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 no-padding-left">
                    <div class="footer_menu_inner footer_contact_form">
                        <input type="hidden" value=" {{csrf_token()}}">
                        <h2>Contact Us:</h2>
                        <input type="text" class="form-control" placeholder="Your name">
                        <input type="emali" class="form-control" placeholder="Your email">
                        <textarea id="" cols="30" rows="5" class="form-control" placeholder="Message"></textarea>
                        <button>SUBMIT</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer_social">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy_right_inner">
                        <p>WooStudy © 2018. All rights reserved.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="copy_right_inner text-right">
                        <p><a href="#">Terms & Conditions</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>