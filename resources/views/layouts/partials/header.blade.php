<header class="header_area">
    @include('layouts.partials.top-header')
    <nav class="navbar navbar-dark navbar-expand-md justify-content-between">
        <div class="container custom_container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-nav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse dual-nav w-50 order-1 order-md-0">
                <ul class="navbar-nav">


                    <li class="nav-item active">
                        @auth
                            <a class="nav-link"
                               href="{{ url('profile/'.auth()->user()->user_name.'/timeline') }}">Home</a>
                        @else
                            <a class="nav-link" href="{{ url('/') }}">Home</a>

                        @endauth
                    </li>

                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('profile/'.auth()->user()->user_name) }}">Profile</a>
                        </li>
                    @endauth
                    <li class="nav-item">
                        <a class="nav-link" href="">Connections</a>
                    </li>
                </ul>
            </div>
            <a href="{{ url('/') }}" class="navbar-brand mx-auto d-block text-center order-0 order-md-1 w-25">
                <img src="{{asset('images/logo.png')}}" alt=""></a>
            <div class="navbar-collapse collapse dual-nav w-50 order-2 right_nav">
                <ul class="nav navbar-nav ml-auto">
                    @auth
                        <li class="nav-item">
                            <a class="nav-link"
                               href="{{ url('profile/'.auth()->user()->user_name.'/chatting') }}">Chat</a>
                        </li>
                    @endauth
                    <li class="nav-item">
                        <a class="nav-link" href="">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">About Us</a>
                    </li>
                </ul>
            </div>
            <div class="header_search">
                <a href="#"><img src="{{asset('images/search_ic.png')}}" alt=""></a>
                {{-- @auth
                     <a href="{{url('profile/'.auth()->user()->user_name.'/notifications')}}"><i class="fas fa-bell"></i>
                         <span>{{auth()->user()->unreadNotifications()->count()}}</span></a>
                 @endauth--}}
            </div>
        </div>
    </nav>
</header>