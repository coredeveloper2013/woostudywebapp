      <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
          <div class="container">
              <a class="navbar-brand" href="{{ url('/') }}">
                <img src='{{ asset('images/static/logo.png') }}' class="img-responsive" style="height: 60px" alt=''/>
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                  <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <!-- Left Side Of Navbar -->
                  <form class="form-inline my-2 my-lg-0 mx-4">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                  </form>
                  <ul class="navbar-nav">

                      @auth
                      <li class="nav-item">
                          <a class="nav-link" href="{{ url('profile', ['user_name' => auth()->user()->user_name]) }}">Profile</a>
                      </li>
                      @endauth
                      <li class="nav-item">
                          <a class="nav-link" href="#">Home</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#">Connection</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#">Blog</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#">About Us</a>
                      </li>
                      @guest
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                          </li>
                          <li class="nav-item">
                              @if (Route::has('register'))
                                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                              @endif
                          </li>
                      @endguest
                  </ul>

                  <!-- Right Side Of Navbar -->
                  <ul class="navbar-nav ml-auto">
                      @auth
                          <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  {{ Auth::user()->name }} <span class="caret"></span>
                              </a>

                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                                 {{-- <a href=""  class="dropdown-item">Change Password</a>--}}
                              </div>
                          </li>
                      @endauth
                  </ul>
              </div>
          </div>
      </nav>







