<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{env('app.name')}}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css_stack')
</head>

<body data-app_domain={{request()->root()}}>

<div id="app">

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please
        <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- START HEADER SECTION-->
@include('layouts.partials.header')
<!-- END HEADER SECTION-->


    <!-- START BANNER SECTION-->
@yield('banner')
<!-- END BANNER SECTION-->


    <!-- START MAIN SECTION-->
@yield('content')
<!-- END MAIN SECTION-->


    <!--START FOOTER SECTION-->
@include('layouts.partials.footer')
<!-- END FOOTER SECTION -->
</div>

<script src='{{ asset('js/app.js') }}'></script>

@stack('js_stack')
@auth
    <script>
        window.auth = <?= json_encode( auth()->user()); ?>
    </script>
@endauth

</body>
</html>


