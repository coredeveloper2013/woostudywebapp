<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/metismenu/dist/metisMenu.min.css">
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/main.css') }}" rel="stylesheet"> --}}
    @stack('css_stack')
</head>
<body data-app_domain={{request()->root()}} class="bg-blue">
<div id="app">
    @include('layouts.menu')
    <main class="py-4">
        @yield('content')
    </main>
</div>
<script src='{{ asset('js/vendor/jquery.min.js') }}'></script>
<script src='{{ asset('js/vendor/popper.min.js') }}'></script>
<script src='{{ asset('js/vendor/bootstrap.min.js') }}?js=<?php echo uniqid(); ?>'></script>
<script src='{{ asset('js/vendor/metis_menu.min.js') }}'></script>
<script src='{{ asset('js/app.js') }}'></script>

@stack('js_stack')
</body>
</html>
