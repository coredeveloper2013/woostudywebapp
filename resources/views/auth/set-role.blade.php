@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center my-4">
                            <img src='{{ asset('images/static/logo.png') }}' class="img-responsive" style="height: 60px"
                                 alt=''/>
                        </div>
                        <!-- /.flex my-4 flex-center -->
                        <form method="POST" action="{{ route('setRole') }}">
                            @csrf
                            @if($emailInput)
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               name="email" value="{{ old('email') }}" required autofocus>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="role"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Who are you') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name='role_id' id='role' class="form-control">
                                        @foreach ($roles as $role)
                                            <option value='{{$role->id}}'>{{$role->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css_stack')
    <style>
        .social-connect {
            padding-top: 20px;
        }
    </style>
@endpush