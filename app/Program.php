<?php

namespace App;


class Program extends Models
{
    protected $table = 'programs';
    protected $fillable = ['address_id', 'education_level_id', 'institute_id', 'title', 'fee', 'admission_date', 'description'];

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class, 'education_level_id' );
    }

    public function institute()
    {
        return $this->belongsTo( Institute::class, 'institute_id' );
    }

    public function address()
    {
        return $this->belongsTo( Address::class, 'address_id' );
    }
}
