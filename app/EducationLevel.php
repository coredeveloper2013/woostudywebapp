<?php

namespace App;


class EducationLevel extends Models
{
    protected $fillable = ['id', 'title', 'status', 'created_by', 'approved_by', 'approved_at'];

    public function educations()
    {
        return $this->hasMany( Education::class );
    }

    public function courses()
    {
        return $this->hasMany( Course::class );
    }
}
