<?php

namespace App;


class Course extends Models
{
    protected $table = 'courses';
    protected $fillable = ['education_level_id', 'user_id', 'title', 'fee', 'starting_date', 'description', 'licence_required', 'licence_id'];

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function licence()
    {
        return $this->belongsTo( Licence::class );
    }

    public function serviceModes()
    {
        return $this->hasMany( CourseServiceMode::class );
    }
}
