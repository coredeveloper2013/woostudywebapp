<?php

function getLastPartOfURL($url)
{
    $end = array_slice(explode('/', rtrim($url, '/')), -1)[0];

    return $end;
}

function extractValueFromArray($array, $field, $defaultValue = "")
{
    if (isset($array[$field]))
        return $array[$field];

    return $defaultValue;
}

function generateHighChartBarJson($data)
{
    if (isset($data) && count($data) > 0) {
        foreach ($data as $key => $value) {
            $dataX[] = $value;
            $dataY[] = $key;
        }
    }

    $results = array();
    $results['xdata'] = $dataX;
    $results['ydata'] = $dataY;

    return $results;
}

function convertToThousand($n, $withoutDecimal = false)
{
    // first strip any formatting;
    //$n = (0+str_replace(",","",$n));

    // is this a number?
    if (!is_numeric($n)) return false;

    // now filter it;
    if (!$withoutDecimal) {
        if ($n > 1000000000000) return round(($n / 1000000000000), 1) . 'T';
        else if ($n > 1000000000) return round(($n / 1000000000), 1) . 'B';
        else if ($n > 1000000) return round(($n / 1000000), 1) . 'M';
        else if ($n > 1000) return round(($n / 1000), 1) . 'K';
        return number_format($n, 1);
    } else {
        if ($n > 1000000000000) return round(($n / 1000000000000)) . 'T';
        else if ($n > 1000000000) return round(($n / 1000000000)) . 'B';
        else if ($n > 1000000) return round(($n / 1000000)) . 'M';
        else if ($n > 1000) return round(($n / 1000)) . 'K';
        return number_format($n);
    }
}


function twitterify($ret)
{
    $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
    $ret = preg_replace("/#(\w+)/", "<a href=\"http://search.twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);

    return $ret;
}

function drillify($url, $ret, $loadData = false)
{
    $base = url('/');
    $base = str_replace("http://", "", $base);
    $url = $base . $url;

    $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" >\\2</a>", $ret);
    $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\">\\2</a>", $ret);

    $ret = preg_replace("/@(\w+)/", "<a href=\"http://" . $url . "/@\\1\" >@\\1</a>", $ret);
    $ret = preg_replace("/#(\w+)/", "<a href=\"http:////" . $url . "/%23\\1\" >#\\1</a>", $ret);

    if ($loadData) {
        $ret = preg_replace("/@(\w+)/", "<a href=\"http://" . $url . "/?loaddata=1&handle=\\1\">@\\1</a>", $ret);
        $ret = preg_replace("/#(\w+)/", "<a href=\"http://" . $url . "?loaddata=1&hashtag=\\1\">#\\1</a>", $ret);
    }

    return $ret;
}


function twitterTime($datetime)
{
    return date("M j, Y g:i A", strtotime($datetime));
}

function time_ago($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime(date($datetime));
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array('y' => 'year', 'm' => 'month', 'w' => 'week', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second',);

    foreach ($string as $k => &$v) {
        if ($diff->$k)
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        else
            unset($string[$k]);
    }

    if (!$full)
        $string = array_slice($string, 0, 1);

    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function sum($carry, $item)
{
    return $carry + $item;
}

function generateBarChartGraphData($keyValuedArr, $numbericConvertion = false)
{
    $graphdata = array();

    foreach ($keyValuedArr as $key => $val) {

        if ($numbericConvertion)
            array_push($graphdata, [convertToThousand($key), (int)$val]);
        else
            array_push($graphdata, [$key, (int)$val]);
    }

    return $graphdata;
}

function generatePieChartGraphData($keyValuedArr, $colors = null)
{
    $graphdata = array();
    $i = 0;

    foreach ($keyValuedArr as $key => $val) {
        $graphdata[$i]['name'] = $key;
        $graphdata[$i]['y'] = (int)$val;
        if (is_array($colors))
            $graphdata[$i]['color'] = $colors[$key];

        $i++;
    }

    return $graphdata;
}

function generateTagCloud($keyValuedArr, $color = null)
{
    $enlargementFactor = 30;
    $data = array();

    foreach ($keyValuedArr as $key => $val) {
        $attr = null;
        $attr = array();
        $attr['text'] = $key;
        $attr['size'] = (int)$val * $enlargementFactor;
        $attr['url'] = url('/') . "/twitter/" . $key;
        if ($color != null)
            $attr['color'] = $color;

        array_push($data, $attr);
    }

    return $data;
}

function createTagCloudArray($keyValuedArr, $color = null)
{
    $enlargementFactor = 30;
    $data = array();
    foreach ($keyValuedArr as $key => $val) {

        $attr = null;
        $attr = array();
        $attr['text'] = $key;
        $attr['size'] = (int)$val * $enlargementFactor;
        $attr['url'] = url('/') . "/twitter/" . $key;
        if ($color != null) {
            $attr['color'] = $color;
        }
        array_push($data, $attr);
    }

    return $data;
}

function natkrsort($array)
{
    $keys = array_keys($array);
    natsort($keys);

    foreach ($keys as $k) {
        $new_array[$k] = $array[$k];
    }

    $new_array = array_reverse($new_array, true);

    return $new_array;
}

function dayTimeInterval($fromDate, $toDate)
{
    $start_date = new DateTime($fromDate);
    $since_start = $start_date->diff(new DateTime($toDate));

    if ($since_start->d >= 1) return $since_start->d . '<br/>Days';
    else if ($since_start->h >= 1) return $since_start->h . '<br/>Hours';
    else if ($since_start->i >= 1) return $since_start->i . '<br/>Minutes';
    else if ($since_start->s >= 1) return $since_start->s . '<br/>Seconds';

    return "";
}