<?php

namespace App;


class Institute extends Models
{
    protected $fillable = ['title', 'education_level_id', 'user_id', 'email', 'phone', 'created_by'];

    public function educations()
    {
        return $this->hasMany( Education::class );
    }

    public function author()
    {
        return $this->hasOne( InstituteAuthor::class );
    }

    public function address()
    {
        return $this->morphMany( Address::class, 'addressable' );
    }

    public function user()
    {
        return $this->belongsTo( User::class );

    }

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class );

    }

    public function programs()
    {
        return $this->hasMany( Program::class );

    }
}
