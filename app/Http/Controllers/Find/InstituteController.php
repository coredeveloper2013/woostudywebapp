<?php

namespace App\Http\Controllers\Find;

use App\Filters\FindInstituteFilter;
use App\Notifications\ProgramInvitationNotification;
use App\Program;
use App\Http\Controllers\Controller;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;

class InstituteController extends Controller
{
    public function __construct(FindInstituteFilter $filter)
    {
        parent::__construct( $filter );
    }

    public function findPrograms()
    {
        $existingInvites = $this->existingFriends();

        return Program::with( 'institute.user', 'address' )->whereNotIn( 'id', $existingInvites )->filter( $this->filter )->paginate( 8 );
    }

    public function sendProgramInvitation(Request $request)
    {
        $model = Program::find( $request->item );
        if ($model->institute && $model->institute->user){
            $model->institute->user->social_connect;

            auth()->user()->befriend( $model, $request->message );
            $model->institute->user->notify( (new ProgramInvitationNotification( $model ))->delay( 5 ) );

            return response()->json( ['user' => $model->institute->user, 'program' => $model] );
        }

        return response()->json( ['message' => 'No data found'] );
    }

    private function existingFriends()
    {
        $recipients = Friendship::where( 'recipient_type', Program::class )->where( 'sender_type', User::class )->where( 'sender_id', auth()->id() )->pluck( 'recipient_id' )->toArray();
        $senders = Friendship::where( 'sender_type', Program::class )->where( 'recipient_type', User::class )->where( 'recipient_id', auth()->id() )->pluck( 'sender_id' )->toArray();

        return array_diff( array_merge( $recipients, $senders ), [auth()->id()] );
    }
}
