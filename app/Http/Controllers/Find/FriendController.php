<?php

namespace App\Http\Controllers\Find;

use App\User;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use App\Http\Controllers\Controller;

class FriendController extends Controller
{
    public function index()
    {
        $friends = $this->friendLists();

        $users = User::whereIn( 'id', $friends )->paginate();

        return response()->json( $users );
    }

    public function count()
    {
        $friends = $this->friendLists();

        $users = count( $friends );

        return response()->json( $users );
    }

    public function friendLists()
    {
        $recipients = Friendship::where( 'recipient_type', User::class )
            ->where( 'sender_type', User::class )
            ->where( function ($q) {
                $q->where( 'sender_id', auth()->id() )
                    ->orWhere( 'recipient_id', auth()->id() );
            } )->where( 'status', Status::ACCEPTED )->get();

        $recipient = $recipients->pluck( 'sender_id' )->unique()->toArray();
        $recipient2 = $recipients->pluck( 'recipient_id' )->unique()->toArray();

        $recipients = array_diff( array_unique( array_merge( $recipient, $recipient2 ) ), [auth()->id()] );

        return $recipients;
    }
}
