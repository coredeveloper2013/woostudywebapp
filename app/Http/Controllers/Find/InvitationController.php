<?php

namespace App\Http\Controllers\Find;

use App\Course;
use App\Http\Controllers\Controller;
use App\Notifications\ProgramInvitationNotification;
use App\Program;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    public function send(Request $request)
    {
        $model = '\App\\' . ucfirst( $request->model );
        /*if ($request->item && $model = $model::find( $request->item )) {
            $model->invitations()->create( ['invited_from' => auth()->id(), 'message' => $request->message, 'status' => 1] );
            $model->institute->user->notify( (new ProgramInvitationNotification( $model ))->delay( 5 ) );
            return response()->json( ['user' => $model->institute->user] );
        }*/
        $model = $model::find( $request->item );
        auth()->user()->befriend( $model, $request->message );
        $model->institute->user->notify( (new ProgramInvitationNotification( $model ))->delay( 5 ) );
        return response()->json( ['user' => $model->institute->user] );
    }

    public function invitationPendingList()
    {
        $recipients = Friendship::where( 'recipient_type', User::class )
            ->where( 'recipient_id', auth()->id() )
            ->whereStatus( Status::PENDING )->paginate( 8 );

        $recipients->each( function ($value) {
            $field = new $value->sender_type();
            $model = $field->find( $value->sender_id );

            if ($model->address) {
                $model->address;
            }
            //dd($model->address()->exists());
            $value->setRelation( 'model', $model );

        } );

        return response()->json( $recipients );
    }

    public function invitationPendingProgramList()
    {
        if (auth()->user()->institute) {
            $programsIds = Program::whereInstituteId( auth()->user()
                ->institute->id )->pluck( 'id' )->toArray();

            $recipients = Friendship::where( 'recipient_type', Program::class )
                ->whereIn( 'recipient_id', $programsIds )
                ->whereStatus( Status::PENDING )->paginate( 8 );

            $recipients->each( function ($value) {
                $field = new $value->sender_type();
                $model = $field->find( $value->sender_id );
                if ($model && $model->address) {
                    $model->address;
                }
                //dd($model->address()->exists());
                $value->setRelation( 'model', $model );

                $programModel = new $value->recipient_type();
                $program = $programModel->find( $value->recipient_id );
                if ($program)
                    $value->setRelation( 'program', $program );

            } );


            return response()->json( $recipients );
        }
    }

    public function invitationPendingProgramFirst()
    {
        if (auth()->user()->institute) {
            $programsIds = Program::whereInstituteId( auth()->user()
                ->institute->id )->pluck( 'id' )->toArray();

            $recipient = Friendship::where( 'recipient_type', Program::class )
                ->whereIn( 'recipient_id', $programsIds )
                ->whereStatus( Status::PENDING )
                ->latest()->first();

            $field = new $recipient->sender_type();
            $model = $field->find( $recipient->sender_id );
            if ($model && $model->address)
                $model->address;

            $recipient->setRelation( 'model', $model );
            $programModel = new $recipient->recipient_type();
            $program = $programModel->find( $recipient->recipient_id );
            if ($program)
                $recipient->setRelation( 'program', $program );

            return response()->json( $recipient );
        }
    }

    public function invitationPendingCourseList()
    {
        $courseIds = Course::whereUserId( auth()->id() )->pluck( 'id' )->toArray();
        $recipients = Friendship::where( 'recipient_type', Course::class )
            ->whereIn( 'recipient_id', $courseIds )
            ->whereStatus( Status::PENDING )->paginate( 8 );
        //$recipients = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->paginate( 8 );

        $recipients->each( function ($value) {
            $field = new $value->sender_type();
            $model = $field->find( $value->sender_id );
            $model->address = auth()->user()->address;
            $value->setRelation( 'model', $model );

            $courseModel = new $value->recipient_type();
            $course = $courseModel->find( $value->recipient_id );

            if ($course)
                $value->setRelation( 'course', $course );

        } );

        return response()->json( $recipients );
    }

    public function invitationPendingCourseFirst()
    {
        $courseIds = Course::whereUserId( auth()->id() )->pluck( 'id' )->toArray();
        if ($courseIds) {
            $recipient = Friendship::where( 'recipient_type', Course::class )
                ->whereIn( 'recipient_id', $courseIds )
                ->whereStatus( Status::PENDING )->latest()->first();
            if ($recipient) {
                $field = new $recipient->sender_type();
                $model = $field->find( $recipient->sender_id );
                $model->address = auth()->user()->address;
                $recipient->setRelation( 'model', $model );
                $courseModel = new $recipient->recipient_type();
                $course = $courseModel->find( $recipient->recipient_id );

                if ($course)
                    $recipient->setRelation( 'course', $course );

                return response()->json( $recipient );
            }
        }
        return response()->json( ['message' => 'No data found'] );

    }

    public function invitationPendingStudentList()
    {
        $recipients = Friendship::where( 'recipient_id', auth()->id() )
            ->whereStatus( Status::PENDING )->paginate( 8 );

        $recipients->each( function ($value) {
            $field = new $value->sender_type();
            $model = $field->find( $value->sender_id );

            if ($model && $model->address) {
                $model->address;
            }

            if ($model && $model->institute()) {
                $model->institute;
            }


            //dd($model->address()->exists());
            $value->setRelation( 'model', $model );

        } );

        return response()->json( $recipients );
    }

    public function invitationPendingStudentFirst()
    {
        $recipient = Friendship::where( 'recipient_id', auth()->id() )->latest()
            ->whereStatus( Status::PENDING )->first();
        if ($recipient) {
            $field = new $recipient->sender_type();
            $model = $field->find( $recipient->sender_id );

            if ($model && $model->address)
                $model->address;

            if ($model && $model->institute())
                $model->institute;

            //dd($model->address()->exists());
            $recipient->setRelation( 'model', $model );

            return response()->json( $recipient );
        }

        return response()->json( ['message' => 'No data found'] );
    }

    public function acceptRequest(Request $request)
    {
        $friend = Friendship::find( $request->id );
        if ($request->status) {
            $friend->status = Status::ACCEPTED;
            $friend->save();

            return response()->json( ['message' => 'Successfully accept the request'] );
        }
    }

    public function acceptAllRequest()
    {
        $pendingRequests = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->pluck( 'id' );
        $acceptAll = Friendship::whereIn( 'id', $pendingRequests )->update( ['status' => Status::ACCEPTED] );

        return response()->json( ['message' => 'Successfully accept all invitations'] );
    }

    public function ignoreRequest(Request $request)
    {
        $friend = Friendship::find( $request->id );
        $friend->status = Status::DENIED;
        $friend->save();

        return response()->json( ['message' => 'Successfully updated'] );
    }

    public function ignoreAllRequest()
    {
        $pendingRequests = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->pluck( 'id' );
        $ignoreAll = Friendship::whereIn( 'id', $pendingRequests )->update( ['status' => Status::DENIED] );

        return response()->json( ['message' => 'Successfully accept the request'] );
    }
}
