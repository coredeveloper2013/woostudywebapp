<?php

namespace App\Http\Controllers;

use App\ProfileOption;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index($slug)
    {
        $user = User::where( 'user_name', $slug )->firstOrFail();
        return view( 'frontend.profiles.index', compact( 'user' ) );
    }

    public function get_auth_user_data()
    {
        $user = User::with( 'role', 'address' )->where( 'id', auth()->id() )->first();

        if (!$user->isSchool()) {
            $user->educations;
            if ($user->educations->count()) {
                foreach ($user->educations as $institute) {
                    $institute->institute;
                }
            }

        }
        if ($user->isSchool()) {
            $user->institute;
            optional( $user->institute )->programs;
            optional( $user->institute )->educationLevel;
            $user->instituteAuthor;
            optional( $user->instituteAuthor )->department;
            optional( $user->instituteAuthor )->instituteRole;
        }

        return $user;
    }

    function get_profile_options($collection, $fields)
    {
        $final_values = [];
        $fields = ['palash', 'sumon'];
        foreach ($fields as $value) {
            $final_values[$value] = '';
        }

        foreach ($users as $user) {
            if (in_array( $user->user_name, $fields )) {
                $final_values[$user->user_name] = $user->name;
            }
        }
        return $final_values;
    }

    public function generate_empty_array_value($fields)
    {

        $final_values = [];
        foreach ($fields as $key) {
            $final_values[$key] = '';
        }
        return $final_values;
    }

    public function get_profile_personal_info($profile_id)
    {
        // name, user, gender, date_of_birth
        $user = User::find( $profile_id );
        $final_values = [];
        $fields = ['gender', 'date_of_birth'];
        $final_values = $this->get_values_from_options_table_by_fields( $fields, $profile_id );
        $final_values['user_name'] = $user->user_name;
        $final_values['name'] = $user->name;
        return $final_values;

    }

    public function post_profile_personal_info()
    {
        $user = auth()->user();
        $date_of_birth = request( 'date_of_birth' );
        $gender = request( 'gender' );
        $name = request( 'name' );
        $user_name = request( 'user_name' );
        $message = [];
        $might_different_user = User::where( 'user_name', $user_name )->first();

        if ($might_different_user) {
            if ($might_different_user->id != $user->id) {
                $message['error'] = 'user_name all ready taken exists';
                return $message;
            }
        }

        $user->name = $name;
        $user->user_name = $user_name;
        $user->save();
        $this->set_or_update_profile_options( 'date_of_birth', $date_of_birth );
        $this->set_or_update_profile_options( 'gender', $gender );
        $message['success'] = 'Profile updated successfully';
        return $message;
    }

    public function set_or_update_profile_options($key, $value)
    {

        $user_id = auth()->id();
        $option = ProfileOption::where( 'option_key', $key )->where( 'user_id', $user_id )->first();
        if ($option) {
            $option->option_value = $value;
            $option->save();
        } else {
            ProfileOption::create( [
                'user_id' => $user_id,
                'option_key' => $key,
                'option_value' => $value,
            ] );
        }
    }

    public function set_or_update_profile_options_by_fields($fields)
    {
        $option_fields = [];
        foreach ($fields as $value) {
            $option_fields[$value] = request( $value );
        }
        foreach ($option_fields as $key => $value) {
            $this->set_or_update_profile_options( $key, $value );
        }
        return true;
    }


    public function get_values_from_options_table_by_fields($fields, $id)
    {
        $final_values = [];
        $user = User::find( $id );
        $options = ProfileOption::where( 'user_id', $user->id )->get();
        $final_values = $this->generate_empty_array_value( $fields );
        foreach ($options as $option) {
            if (in_array( $option->option_key, $fields )) {
                $final_values[$option->option_key] = $option->option_value;
            }
        }
        return $final_values;
    }

    public function get_contact_details($id)
    {
        $fields = ['country', 'city', 'address', 'lat', 'lng'];
        return $this->get_values_from_options_table_by_fields( $fields, $id );
    }


    public function get_social_media_details($id)
    {
        $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
        return $this->get_values_from_options_table_by_fields( $fields, $id );
    }

    public function post_contact_details()
    {
        $fields = ['country', 'city', 'address', 'lat', 'lng'];
        $this->set_or_update_profile_options_by_fields( $fields );
        return response()->json( [
            'success' => 'update information successfully'
        ] );

    }

    public function post_social_media_details()
    {
        $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
        $this->set_or_update_profile_options_by_fields( $fields );
        return response()->json( [
            'success' => 'update information successfully'
        ] );
    }

    public function publicProfile($profileId)
    {
        $user=User::with( 'role', 'address' )->whereUserName( $profileId )->first();

        if (!$user->isSchool()) {
            $user->educations;
            if ($user->educations->count()) {
                foreach ($user->educations as $institute) {
                    $institute->institute;
                }
            }
        }
        if ($user->isSchool()) {
            $user->institute;
            optional( $user->institute )->programs;
            optional( $user->institute )->educationLevel;
            $user->instituteAuthor;
            optional( $user->instituteAuthor )->department;
            optional( $user->instituteAuthor )->instituteRole;
        }

        if ($user->isTutor()) {
            $user->courses;
            if ($user->courses->count()) {
                foreach ($user->courses as $course) {
                    $course->educationLevel;
                    $course->licence;
                    $course->serviceModes;
                }
            }
        }

        return $user;
    }

}
