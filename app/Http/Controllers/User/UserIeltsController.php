<?php

namespace App\Http\Controllers\User;

use App\Ielts;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserIeltsController extends Controller
{
    public function show($id)
    {
        $user = User::find( $id );
        return Ielts::with( 'examLevel' )->where( 'user_id', $user->id )->get();
    }

    public function store()
    {
        $fields = ["ielts_exam_level_id", "score", "taken_on", "grade"];
        $data = [];
        foreach ($fields as $field) {
            $data[$field] = request( $field );
        }
        $data['user_id'] = auth()->id();

        return Ielts::create( $data );

    }

    public function destroy($id)
    {
        $ielts = Ielts::find( $id );
        $ielts->delete();
        return response()->json( ['success' => 'deleted successfully'] );
    }

    public function update(Request $request, $id)
    {
        $ielts = Ielts::find( $id );
        $ielts->fill( $request->all() )->save();

        return response()->json( [
            'success' => 'Ielts Details update successfully',
        ] );
    }
}
