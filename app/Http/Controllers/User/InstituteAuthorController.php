<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\InstituteAuthor;
use Illuminate\Http\Request;

class InstituteAuthorController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $authUer = auth()->user();
        $data = $request->except( 'departments' );
        $data['created_by'] = $authUer->id;
        $data['user_id'] = $authUer->id;
        $data['institute_id'] = $authUer->institute ? $authUer->institute->id : null;

        return InstituteAuthor::create( $data );
    }


    public function show($instituteAuthor)
    {
        InstituteAuthor::whereCreatedBy( auth()->id() )->with( 'department', 'instituteRole' )->find( $instituteAuthor );
    }


    public function edit(InstituteAuthor $instituteAuthor)
    {
        //
    }


    public function update(Request $request, InstituteAuthor $instituteAuthor)
    {
        //
    }

    public function destroy(InstituteAuthor $instituteAuthor)
    {
        //
    }
}
