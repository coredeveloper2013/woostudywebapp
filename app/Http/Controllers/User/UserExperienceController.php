<?php

namespace App\Http\Controllers\User;

use App\Experience;
use App\Http\Requests\ExperienceRequest;
use App\Http\Controllers\Controller;

class UserExperienceController extends Controller
{
    public function show($id)
    {
        return Experience::whereUserId( $id )->with('institute')->get();
    }

    public function store(ExperienceRequest $request)
    {
        $experience = new Experience();
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $experience = $experience->fill( $data )->save();
        if ($experience) {
            return response()->json( ['success' => 'Experiences create successfully'] );
        }
    }

    public function update(ExperienceRequest $request, $id)
    {
        $experience = Experience::whereUserId( auth()->id() )->find( $id );
        $experience = $experience->fill( $request->all() )->save();
        if ($experience) {
            return response()->json( ['success' => 'Experiences update successfully'] );
        }
    }

    public function destroy($id)
    {
        $experience = Experience::whereUserId( auth()->id() )->find( $id );
        $experience = $experience->delete();
        if ($experience) {
            return response()->json( ['success' => 'Experiences delete successfully'] );
        }
    }
}
