<?php

namespace App\Http\Controllers\User;

use App\Course;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserCourseController extends Controller
{
    public function show($id)
    {
        return Course::whereUserId( $id )->with( 'educationLevel', 'licence', 'serviceModes' )->get();
    }

    public function store(Request $request)
    {
        $course = new Course();
        $data = $request->all();
        $data['user_id'] = auth()->id();
        $course = $course->fill( $data )->create( $data );
        if ($course) {
            $service_modes = array_map( function ($mode) {
                return array('mode' => $mode);
            }, $request->service_modes );
            $course->serviceModes()->createMany( $service_modes );
        }

        return $course;
    }

    public function update(Request $request, $id)
    {
        $course = Course::whereUserId( auth()->id() )->find( $id );
        $courseUpdate=$course;
        $courseUpdate->fill( $request->all() )->save();

        if ($course) {
            $service_modes = array_map( function ($mode) {
                return array('mode' => $mode);
            }, $request->service_modes );
            $course->serviceModes()->delete();
            $course->serviceModes()->createMany( $service_modes );
        }

        if ($course) {
            return response()->json( ['success' => 'Course offer create successfully'] );
        }

    }

    public function destroy($id)
    {
        $course = $course = Course::whereUserId( auth()->id() )->find( $id );
        $course->serviceModes()->delete();
        $course = $course->delete();

        if ($course) {
            return response()->json( ['success' => 'Delete successfully'] );
        }

    }
}
