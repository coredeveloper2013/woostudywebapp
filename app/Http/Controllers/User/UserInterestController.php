<?php

namespace App\Http\Controllers\User;

use App\Interest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserInterestController extends Controller
{

    public function show($id)
    {
        $user = User::find( $id );
        return Interest::where( 'user_id', $user->id )->get();
    }

    public function store()
    {
        $fields = ["title", "since"];
        $data = [];
        foreach ($fields as $field) {
            $data[$field] = request( $field );
        }
        $data['user_id'] = auth()->id();
        return Interest::create( $data );

    }

    public function destroy($id)
    {
        $ielts = Interest::find( $id );
        $ielts->delete();
        return response()->json( ['success' => 'deleted successfully'] );

    }

    public function update($id)
    {

        $fields = ["title", "since"];
        $data = [];
        foreach ($fields as $field) {
            $data[$field] = request( $field );
        }
        Interest::where( 'id', $id )->update( $data );

        return response()->json( [
            'success' => 'Ielts Details update successfully',
        ] );
    }
}
