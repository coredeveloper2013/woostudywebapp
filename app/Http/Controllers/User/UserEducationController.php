<?php

namespace App\Http\Controllers\User;

use App\Education;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserEducationController extends Controller
{

    public function show($id)
    {
        $user = User::find( $id );
        return Education::with( 'institute', 'educationLevel' )->where( 'user_id', $user->id )->get();
    }

    public function store()
    {
        $fields = ['title', 'grade', 'institute_id', 'completion_date', 'percentage', 'education_level_id'];

        $data = [];

        foreach ($fields as $field) {
            $data[$field] = request( $field );
        }
        $data['user_id'] = auth()->id();

        return Education::create( $data );
    }

    public function destroy($id)
    {
        $education = Education::find( $id );
        $education->delete();
        return response()->json( ['success' => 'deleted successfully'] );
    }

    public function update(Request $request, $id)
    {
        $education = Education::find( $id );

        $education->fill( $request->all() )->save();

        return response()->json( [
            'success' => 'Education Details update successfully'
        ] );
    }
}
