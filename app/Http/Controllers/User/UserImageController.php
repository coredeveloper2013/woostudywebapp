<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\MediaController;
use App\Http\Requests\MediaImageRequest;
use App\User;
use App\Http\Controllers\Controller;

class UserImageController extends Controller
{
    public function store(MediaImageRequest $request)
    {
        $image = (new MediaController())->ImageUpload( $request );

        if (auth()->user()->profileImage()) {
            $this->destroy( auth()->user()->profileImage()['id'] );
        }

        $uploadImage = auth()->user()->medias()->create( $image );

        if ($uploadImage && $uploadImage->filename) {
            return response()->json( [
                'success' => 'Image upload successfully',
                'url' => '/storage/uploads/images/' . $uploadImage->filename
            ] );
        }

    }

    public function show($id)
    {
        $user = User::find( $id );
        return $user->avater;
    }

    public function destroy($id)
    {
        $image = (new MediaController())->delete( [$id] );

        return response()->json( [
            'success' => 'Image Deleted successfully',
            'status' => $image,
        ] );
    }
}
