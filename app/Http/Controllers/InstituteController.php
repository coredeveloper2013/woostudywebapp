<?php

namespace App\Http\Controllers;

use App\Institute;
use App\InstituteAuthor;
use App\User;
use Illuminate\Http\Request;

class InstituteController extends Controller
{

    public function index()
    {
        return Institute::all();
    }


    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {
        $institute = new Institute();
        $data = $request->all(); //all request data
        $data['user_id'] = auth()->id();
        $data['created_by'] = $data['user_id'];
        $institute->fill( $data ); //fill all data
        $institute->save(); //save to database
        if (!$institute->author) {
            (new \App\InstituteAuthor)->where( 'created_by', auth()->id() )->update( ['institute_id' => $institute->id] );
        }
        return $institute;
    }


    public function show($id)
    {
        $institute = Institute::whereUserId( $id )->first();
        return response()->json( $institute );
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $institute = Institute::findOrFail( $id ); // find model
        $data = $request->all(); //all request data
        $institute->fill( $data ); //fill all data
        $institute->save(); //save to database
        return $institute;
    }


    public function destroy($id)
    {
        //
    }
}
