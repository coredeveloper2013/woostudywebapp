<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Find\FriendController;
use App\Http\Requests\MediaVideoRequest;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index(FriendController $friendController)
    {
        $friends = $friendController->friendLists();

        array_push( $friends, auth()->id() );

        $posts = Post::whereStatus( true )
            ->with('video')
            ->whereIn( 'user_id', $friends )
            ->latest( 'created_at' )
            ->with( ['user', 'comments' => function ($q) {
                return $q->latest( 'created_at' )->with( 'user' );
            }] )->paginate();

        $posts->each( function ($data) {
            if ($data->user->role_id === 3) {
                return $data->user->institute;
            };
        } );


        return response()->json( $posts );
    }

    public function store(Request $request)
    {
        $post = Post::create( [
            'body' => $request->body,
            'user_id' => auth()->id()
        ] );
        $post->user;

        return response()->json( $post );
    }


    public function update(Request $request, Post $post)
    {
        $post->fill( $request->all() )->save();

        return response()->json( ['message' => 'update successfully'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->comments()->detauch();
        $post->delete();

        return response()->json( ['message' => 'Delete successfully'] );
    }

    public function videoPost(MediaVideoRequest $request)
    {

        $post = Post::create( [
            'body' => $request->body,
            'user_id' => auth()->id()
        ] );

        if ($post) {
            $videos = (new MediaController())->videoUpload( $request );
            $videos['mediable_type'] = Post::class;
            $post->media()->create( $videos );
            $post->user;
            $post->video;

            return response()->json( $post );
        }
    }
}
