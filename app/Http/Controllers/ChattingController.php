<?php

namespace App\Http\Controllers;

use App\Chatting;
use App\Events\SendMessageEvent;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;

class ChattingController extends Controller
{
    public function sendMessage(Request $request)
    {
        $message = Chatting::create( [
            'message' => $request->message,
            'from' => auth()->id(),
            'to' => $request->friend_id
        ] );
        if($user=User::find($request->friend_id)){
            if(!$user->online){
                if(!$user->unreadNotifications()->whereType(NewMessageNotification::class)->first())
                $user->notify( (new NewMessageNotification( $user, $message ))->delay( 5 ) );
            }
        }
        //broadcast(new NewMessage($message))->toOthers();

        broadcast( new SendMessageEvent( $message ) )->toOthers();

        return response()->json( $message );
    }


    public function history($friendId)
    {
        $histories = Chatting::where( function ($q) use ($friendId) {
            $q->whereFrom( auth()->id() )->whereTo( $friendId );
        } )->orWhere( function ($q) use ($friendId) {
            $q->whereTo( auth()->id() )->whereFrom( $friendId );
        } )->limit( 12 )->get();

        return response()->json( $histories );
    }


    public function update(Request $request, Chatting $chatting)
    {
        $chatting->message = $request->message;
        $chatting->save();
        return response()->json( ["result" => "updated message"] );
    }


    public function destroy(Chatting $chatting)
    {
        try {
            $chatting->delete();
            return response()->json( ["result" => "deleted message"] );
        } catch (\Exception $e) {
        }
        return response()->json( ["result" => "deleted message"] );
    }

    public function chatFriendLists()
    {
        $friends = Chatting::where( 'to', auth()->id() )
            ->orWhere( 'from', auth()->id() )
            ->latest( 'created_at' )
            ->limit( 10 )
            ->distinct( ['from', 'to'] )->get();

        $friend = $friends->pluck( 'from' )->unique()->toArray();
        $friend2 = $friends->pluck( 'to' )->unique()->toArray();

        $users = array_diff( array_unique( array_merge( $friend, $friend2 ) ), [auth()->id()] );

        $recentUsers = User::whereIn( 'id', $users )->get();

        if ($friends) {
            $recentUsers->map( function ($value, $key) use ($friends) {
                $data = $friends->where( 'from', $value->id )->first();
                $value->setRelation( 'lastChat', $data );
            } );
        }
        
        return $recentUsers;
    }

}
