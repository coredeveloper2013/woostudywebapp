<?php

namespace App\Http\Controllers;

use App\IELTSExamLevel;
use Illuminate\Http\Request;

class IeltsExamLevelController extends Controller
{
    public function index()
    {
        return IELTSExamLevel::all();
    }


    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {
        $ieltsExamLevel = new IELTSExamLevel();
        $data = $request->all(); //all request data
        $data['created_by'] = auth()->id();
        $ieltsExamLevel->fill( $data ); //fill all data
        $ieltsExamLevel->save(); //save to database
        return $ieltsExamLevel;
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $ieltsExamLevel = IELTSExamLevel::findOrFail( $id ); // find model
        $data = $request->all(); //all request data
        $ieltsExamLevel->fill( $data ); //fill all data
        $ieltsExamLevel->save(); //save to database
        return $ieltsExamLevel;
    }


    public function destroy($id)
    {
        //
    }
}
