<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function store(Request $request)
    {
        $post = Post::whereUserId( auth()->id() )->whereId( $request->post_id )->first();
        $comment = $post->comments()->create( ['body' => $request->body, 'user_id' => auth()->id()] );
        $comment->user;
        return response()->json( $comment );
    }


    public function update(Request $request, Comment $comment)
    {
        $comment->message = $request->message;
        $comment->update();

        return response()->json( ['message' => 'update successfully'] );
    }


    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response()->json( ['message' => 'Delete successfully'] );
    }
}
