<?php
/**
 * Created by PhpStorm.
 * User: alamincse
 * Date: 1/29/2019
 * Time: 9:20 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\MediaImageRequest;
use App\Http\Requests\MediaVideoRequest;
use App\Media;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MediaController
{
    public $baseUpload = 'images/';

    public function upload(Request $request)
    {
        $file = $request->file;
        $image['title'] = $request->title ?? Null;

        if (is_file( $file )) {
            $image['original_filename'] = $file->getClientOriginalName();
            $image['size'] = $file->getSize();
            $image['mime_type'] = $file->getMimeType();
            $image['ext'] = $file->getClientOriginalExtension();;
            $now = DateTime::createFromFormat( 'U.u', microtime( true ) );
            $fileName = $now->format( "d_H_i_s_u" );

            $image['filename'] = $fileName . '.' . $image['ext'];

            Storage::disk( 'local' )->putFileAs(
                'public/uploads/' . $this->baseUpload,
                $file,
                $image['filename']
            );

            $file->move( $this->baseUpload, $image['filename'] );

            return $image;
        }
    }

    public function videoUpload(Request $request)
    {
        $this->baseUpload = 'videos/';

        return $this->upload( $request );
    }

    public function ImageUpload(Request $request)
    {
        $this->baseUpload = 'images/';

        return $this->upload( $request );
    }


    public function delete($files, $path = 'images/')
    {
        foreach ($files as $key => $file) {
            $file = Media::findOrfail( $file );
            if (file_exists( 'storage/uploads/' . $path . $file->filename )) {
                $ds = Storage::delete( 'public/uploads/' . $path . $file->filename );
                if ($ds)
                    $file->delete();
            }
        }

        return true;
    }

}