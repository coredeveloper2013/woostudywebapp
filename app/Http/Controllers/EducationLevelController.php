<?php

namespace App\Http\Controllers;

use App\EducationLevel;
use Illuminate\Http\Request;

class EducationLevelController extends Controller
{
    public function index()
    {
        return EducationLevel::all();
    }


    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {
        $educationLevel = new EducationLevel();
        $data = $request->all(); //all request data
        $data['created_by'] = auth()->id();
        $educationLevel->fill( $data ); //fill all data
        $educationLevel->save(); //save to database
        return $educationLevel;
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $educationLevel = EducationLevel::findOrFail( $id ); // find model
        $data = $request->all(); //all request data
        $educationLevel->fill( $data ); //fill all data
        $educationLevel->save(); //save to database
        return $educationLevel;
    }


    public function destroy($id)
    {
        //
    }
}
