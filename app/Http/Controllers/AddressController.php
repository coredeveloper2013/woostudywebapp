<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->address;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return auth()->user()->address()->create( $request->all() );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function show($address)
    {
        return auth()->user()->address;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $address = $address->fill( $request->all() )->save();
        if ($address) {
            return response()->json( ['success' => 'Update successfully'] );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $address->delete();
        return response()->json( ['success' => 'deleted successfully'] );
    }


    public function get_contact_details($id)
    {
        $fields = ['country', 'city', 'address', 'lat', 'lng'];
        return $this->get_values_from_options_table_by_fields( $fields, $id );
    }


    public function get_social_media_details($id)
    {
        $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
        return $this->get_values_from_options_table_by_fields( $fields, $id );
    }

    public function post_contact_details()
    {
        $fields = ['country', 'city', 'address', 'lat', 'lng'];
        $this->set_or_update_profile_options_by_fields( $fields );
        return response()->json( [
            'success' => 'update information successfully'
        ] );

    }
}
