<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function unreadNotificationCount()
    {
        return auth()->user()->unreadNotifications->count();
    }

    public function index()
    {
        $notifications = auth()->user()->notifications()->paginate();

        return response()->json( $notifications );
    }

    public function unreadNotification()
    {
        return auth()->user()->unreadNotifications;
    }

    public function readNotification()
    {
        return auth()->user()->readNotifications ;
    }

    public function markAsRead($id)
    {
        $read=auth()->user()->unreadNotifications()->find($id)->markAsRead();

        return response()->json( ['message'=>'Successfully set as read'] );
    }

    public function markAsUnRead($id)
    {
        $read=auth()->user()->readNotifications()->find($id)->markAsUnRead();

        return response()->json( ['message'=>'Successfully set as unread'] );
    }
}
