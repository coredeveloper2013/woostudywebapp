<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Program::whereInstituteId( auth()->user()->institute->id )->with( 'educationLevel', 'address' )->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['institute_id'] = auth()->user()->institute ? auth()->user()->institute->id : '';
        $program = Program::create( $data );
        return $program;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $program)
    {

        $program = Program::findOrFail( $program );
        $data = $request->all();
        $data['institute_id'] = auth()->user()->institute ? auth()->user()->institute->id : '';
        $program = $program->fill( $data )->save();
        if ($program) {
            return response()->json( ['success' => 'update successfully'] );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($program)
    {
        $program = Program::find( $program );
        $program->delete();
        return response()->json( ['success' => 'deleted successfully'] );
    }
}
