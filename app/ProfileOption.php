<?php

namespace App;


class ProfileOption extends Models
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo( User::class );
    }
}
