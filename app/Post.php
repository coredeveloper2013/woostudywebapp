<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'body', 'status'];

    public function comments()
    {
        return $this->hasMany( Comment::class );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function media()
    {
        return $this->hasOne( Media::class, 'mediable_id' );
    }

    public function video()
    {
        return $this->media()->where('mediable_type', Post::class);
    }
}
