<?php

namespace App\Events;

use App\Chatting;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $chatting;

    public function __construct(Chatting $chatting)
    {
        $this->chatting = $chatting;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    /* public function broadcastOn()
     {
         return new PresenceChannel( 'chats.' . $this->chatting->friend_id );
     }*/

    public function broadcastOn()
    {
        $chanel = ($this->chatting->from < $this->chatting->to) ? $this->chatting->from . '-' . $this->chatting->to : $this->chatting->to . '-' . $this->chatting->from;

        return new PrivateChannel( 'chats.' . $chanel );
    }
}
