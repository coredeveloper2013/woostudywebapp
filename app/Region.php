<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Models
{
    protected $table = 'regions';
    protected $fillable = ['title', 'slug'];
}
