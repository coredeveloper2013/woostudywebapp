<?php

namespace App;

class Interest extends Models
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsTo( User::class );
    }
}
