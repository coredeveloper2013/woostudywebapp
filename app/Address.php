<?php

namespace App;


class Address extends Models
{
    protected $table = 'addresses';
    protected $fillable = ['country_id', 'state_id', 'city', 'address', 'lat', 'lng', 'addressable_id', 'addressable_type'];

    protected $appends = ['country'];

    public function country()
    {
        return $this->belongsTo( Country::class );
    }

    public function state()
    {
        return $this->belongsTo( State::class );
    }

    public function addressable()
    {
        return $this->morphTo();
    }

    public function getCountryAttribute()
    {
        return $this->country()
            ->whereId($this->country_id)
            ->select( ['title'] )->first();
    }
}
