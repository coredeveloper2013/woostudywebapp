<?php

namespace App;


class Licence extends Models
{
    protected $table = 'licences';

    protected $fillable = ['user_id', 'valid_till', 'title', 'start_date', 'authority'];


    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function courses()
    {
        return $this->hasMany( Course::class );
    }
}
