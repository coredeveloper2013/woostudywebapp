<?php

namespace App;

use App\Filters\QueryFilter;
use Carbon\Carbon;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use Hootlex\Friendships\Traits\Friendable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasSlug, Friendable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_name', 'role_id'
    ];

    protected $appends = ['avater', 'online'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->role_id == 1;
    }

    public function isStudent()
    {
        return $this->role_id == 2;
    }

    public function isSchool()
    {
        return $this->role_id == 3;
    }

    public function isTutor()
    {
        return $this->role_id == 4;
    }

    public function isCounselor()
    {
        return $this->role_id == 5;
    }

    public function isParent()
    {
        return $this->role_id == 6;
    }

    public function getRole()
    {
        return $this->role->title;
    }


    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom( 'name' )
            ->saveSlugsTo( 'user_name' );
    }

    public function role()
    {
        return $this->belongsTo( Role::class );
    }

    public function educations()
    {
        return $this->hasMany( Education::class );
    }

    public function lastEducation()
    {
        return $this->educations()->latest()->limit( 1 );
    }

    public function lastInterest()
    {
        return $this->interests()->latest()->limit( 1 );
    }

    public function highestEducation()
    {
        return $this->hasOne( Education::class )->oldest( 'created_at' );
    }

    public function lastIelts()
    {
        return $this->ielts()->latest()->limit( 1 );
    }

    public function ielts()
    {
        return $this->hasMany( Ielts::class );
    }

    public function interests()
    {
        return $this->hasMany( Interest::class );
    }

    public function addEducation($data)
    {
        $data['user_id'] = $this->id;
        return $this->educations->create( $data );
    }

    public function profile_options()
    {
        return $this->hasMany( ProfileOption::class );
    }

    public function social_connect()
    {
        return $this->profile_options()->select( 'option_key', 'option_value' )->whereIn( 'option_key', ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'] );
    }

    /* public function videos()
     {
         return $this->hasMany( Video::class );
     }*/

    public function socialProviders()
    {
        return $this->hasMany( SocialProvider::class );
    }

    public function experiences()
    {
        return $this->hasMany( Experience::class );
    }

    public function getTotalExperienceAttribute()
    {
        $experiences = $this->experiences()->orderBy( 'from_date' )->get();
        $start = $experiences->first()->from_date;
        $end = $experiences->last()->to_date ?? Carbon::now();
        return $start->diffInYears( $end );
    }

    public function licences()
    {
        return $this->hasMany( Licence::class );
    }

    public function courses()
    {
        return $this->hasMany( Course::class );
    }

    public function instituteAuthor()
    {
        return $this->hasOne( InstituteAuthor::class, 'created_by' );
    }

    public function institute()
    {
        if ($this->role_id === 3) {
            return $this->hasOne( Institute::class, 'user_id' );
        }
    }

    public function address()
    {
        return $this->{auth()->user()->isSchool() ? 'morphMany' : 'morphOne'}( Address::class, 'addressable' );
    }

    public function invitations()
    {
        return $this->morphToMany( Friendship::class, 'friend' );
    }

    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply( $query );
    }

    public function chattings()
    {
        return $this->belongsToMany( Chatting::class, 'chattings', 'from', 'to' );
    }

    public function befriend(Model $recipient, $message)
    {

        if (!$this->canBefriend( $recipient )) {
            return false;
        }

        $friendship = (new Friendship)->fillRecipient( $recipient )->fill( [
            'status' => Status::PENDING,
            'message' => $message
        ] );

        $this->friends()->save( $friendship );

        Event::fire( 'friendships.sent', [$this, $recipient] );

        return $friendship;

    }

    public function getFrindShipByModel($model, $status = null)
    {
        return Friendship::where( 'recipient_type', $model->getMorphClass() )->orWhere( 'recipient_type', $model->getMorphClass() );

    }


    public function friendList($status = Status::ACCEPTED, $sender = User::class, $recipient = User::class)
    {
        $friends = Friendship::where( function ($q) use ($recipient) {
            $q->where( 'recipient_type', $recipient )->where( 'recipient_id', $this->id );
        } )->orWhere( function ($q) use ($sender) {
            $q->where( 'sender_type', $sender )->where( 'sender_id', $this->id );
        } );
        if ($status)
            $friends->whereStatus( $status );

        $friends->get();

        $friend = $friends->pluck( 'recipient_id' )->unique()->toArray();
        $friend2 = $friends->pluck( 'sender_id' )->unique()->toArray();

        return array_diff( array_unique( array_merge( $friend, $friend2 ) ), [$this->id] );
    }

    public function posts()
    {
        return $this->hasMany( Post::class );
    }

    public function medias()
    {
        return $this->morphMany( Media::class, 'mediable' );
    }

    public function videos()
    {
        return $this->medias()->whereIn( 'mime_type', config( 'media.video' ) );
    }

    public function getAvaterAttribute()
    {
        $data = optional( $this->profileImage() )->filename;

        $default = 'avater.jpg';
        if (!$data) {
            if ($this->isSchool()) {
                $default = 'school.png';
            }
            if ($this->isTutor()) {
                $default = 'tutor.png';
            }
        }
        return '/storage/uploads/images/' . ($data ?? $default);
    }

    public function profileImage()
    {
        return $this->medias()->whereIn( 'mime_type', config( 'media.image' ) )->latest()->first();
    }

    public function getOnlineAttribute()
    {
        return Cache::has('user-is-online-' . $this->id);
    }
}
