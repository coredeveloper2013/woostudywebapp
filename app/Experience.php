<?php

namespace App;

class Experience extends Models
{
    protected $table = 'experiences';
    protected $fillable = ['title','institute_id', 'user_id', 'from_date', 'to_date', 'continue'];

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function institute()
    {
        return $this->belongsTo( User::class );
    }
}
