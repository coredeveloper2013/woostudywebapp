<?php

namespace App;


class Education extends Models
{
    protected $fillable = ['title', 'grade', 'user_id', 'institute_id', 'completion_date', 'percentage', 'education_level_id'];
    protected $table = 'educations';

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function institute()
    {
        return $this->belongsTo( Institute::class );
    }

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class );
    }
}
