<?php

namespace App;


class InstituteAuthor extends Models
{
    protected $table = 'institute_author_user';
    protected $fillable = ['institute_id', 'department_id', 'institute_role_id', 'name', 'email', 'phone', 'mobile','created_by'];

    public function institute()
    {
        return $this->belongsTo( Institute::class );
    }

    public function department()
    {
        return $this->belongsTo( Department::class );
    }

    public function instituteRole()
    {
        return $this->belongsTo( InstituteRole::class );
    }

    public function createdBy()
    {
        return $this->belongsTo( User::class, 'created_by' );
    }
}
