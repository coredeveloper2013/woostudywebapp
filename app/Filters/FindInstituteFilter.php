<?php

namespace App\Filters;


class FindInstituteFilter extends FindInvitationFilter
{
    public function institute($title)
    {
        return $this->builder->whereHas( 'institute', function ($query) use ($title) {
            $query->where( 'title', 'like', '%' . $title . '%' );
        } );
    }

    public function role($id)
    {
        return $this->builder->whereHas( 'institute.author', function ($query) use ($id) {
            $query->whereInstituteRoleId( $id );
        } );
    }

    public function department($id)
    {
        return $this->builder->whereHas( 'institute.author', function ($query) use ($id) {
            $query->whereDepartmentId( $id );
        } );
    }

    public function scholarship()
    {
        return $this->builder;
    }

    public function range(array $prices)
    {
        $this->builder->whereBetween( 'fee', $prices );
    }
}