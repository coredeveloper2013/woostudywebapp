<?php

namespace App\Filters;


class FindInvitationFilter extends DataFilter
{
    public function country($id)
    {
        return $this->builder->whereHas( 'address', function ($query) use ($id) {
            $query->whereCountryId( $id );
        } );
    }

    public function state($id)
    {
        return $this->builder->whereHas( 'address', function ($query) use ($id) {
            $query->whereStateId( $id );
        } );
    }

    public function region($id)
    {
        return $this->builder->whereHas( 'address', function ($query) use ($id) {
            $query->whereHas( 'country', function ($q) use ($id) {
                $q->whereRegionId( $id );
            } );
        } );
    }

    public function title($title)
    {
        return $this->builder->where( 'title', 'like', '%' . $title . '%' );
    }

    public function distance($distance)
    {
        $radius = (double)$distance;
        $lat = (float)request()->lat;
        $lng = (float)request()->lng;
        $unit = 6378.10;

        return $this->builder->whereHas( 'address', function ($query) use ($radius, $lat, $lng, $unit) {
            $query->having( 'distance', '<=', $radius )
                ->selectRaw( ("id,lat, lng,
                            ($unit * ACOS(COS(RADIANS($lat))
                                * COS(RADIANS(lat))
                                * COS(RADIANS(lng) - RADIANS($lng))
                                + SIN(RADIANS($lat))
                                * SIN(RADIANS(lat)))) AS distance")
                )->orderBy( 'distance', 'asc' );
        } );
    }
}