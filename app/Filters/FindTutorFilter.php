<?php

namespace App\Filters;


class FindTutorFilter extends FindInvitationFilter
{

    public function program($value)
    {
        $this->builder->whereHas( 'educations', function ($qury) use ($value) {
            $qury->whereTitle( $value );
        } );
    }

    public function teacher($name)
    {
        return $this->builder->whereHas( 'user', function ($q) use ($name) {
            $q->where( 'name', 'like', '%' . $name . '%' );
        } );
    }

    public function interest($title)
    {
        return $this->builder->whereHas( 'user.interests', function ($q) use ($title) {
            $q->where( 'title', 'like', '%' . $title . '%' );
        } );
    }

    public function country($id)
    {
        return $this->builder->whereHas( 'user.address', function ($query) use ($id) {
            $query->whereCountryId( $id );
        } );
    }

    public function state($id)
    {
        return $this->builder->whereHas( 'user.address', function ($query) use ($id) {
            $query->whereStateId( $id );
        } );
    }

    public function region($id)
    {
        return $this->builder->whereHas( 'user.address', function ($query) use ($id) {
            $query->whereHas( 'country', function ($q) use ($id) {
                $q->whereRegionId( $id );
            } );
        } );
    }


    public function distance($distance)
    {
        $radius = (double)$distance;
        $lat = (float)request()->lat;
        $lng = (float)request()->lng;
        $unit = 6378.10;

        return $this->builder->whereHas( 'user.address', function ($query) use ($radius, $lat, $lng, $unit) {
            $query->having( 'distance', '<=', $radius )
                ->selectRaw( ("id,lat, lng,
                            ($unit * ACOS(COS(RADIANS($lat))
                                * COS(RADIANS(lat))
                                * COS(RADIANS(lng) - RADIANS($lng))
                                + SIN(RADIANS($lat))
                                * SIN(RADIANS(lat)))) AS distance")
                )->orderBy( 'distance', 'asc' );
        } );
    }
}