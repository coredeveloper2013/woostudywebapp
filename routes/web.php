<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get( '/profile/{user_name}/{vue_route?}', 'ProfileController@index' )->where( 'vue_route', '(.*)' )->middleware( 'auth' );*/

Route::group( ['namespace' => 'Auth'], function () {
    Route::post( 'login/setRole', 'SocialProviderController@setRole' )->name( 'setRole' );
    Route::get( 'login/{provider}', 'SocialProviderController@redirectToProvider' );
    Route::get( 'login/{provider}/callback', 'SocialProviderController@handleProviderCallback' );
} );

/**
 * JSON End point
 */
require __dir__ . '/jsonend.php';

Route::group( ['namespace' => 'Twitter', 'prefix' => 'twitter'], function () {
    //Set twitter role
    Route::get( 'geo-point', 'TweetController@testGeoPoint' );
    Route::get( 'search-from-db', 'TweetController@searchFromDB' );
    Route::get( 'fetch-twitter-from-db/{id}', 'TweetController@fetchTimeLineTweetsFromDB' );
    Route::get( 'fetch-twitter-from-rows', 'TweetController@fetchTweetsFromRows' );
    Route::get( 'query-by-db', 'TweetController@queryDbByIds' );
    Route::get( 'search-from-db-minimal-data', 'TweetController@searchFromDBMinimalData' );
    Route::get( 'init-tweets-for-twitter-timeline/{id?}', 'TweetController@initTweetsForTwitterTimeLine' );
    Route::get( 'populate-training-data/{query}', 'TweetController@populateTrainingData' );
    Route::get( 'populate-twitter-data', 'TweetController@populateTwitterData' );
    Route::get( 'get-user-timeline/{screenName}', 'TweetController@getUserTimeLine' );
    Route::get( 'tag-tweets-from-aliyan/{tweetIds}', 'TweetController@tagTweetsFromAliyan' );
    Route::get( 'enrich-pending-tweets', 'TweetController@enrichPendingTweets' );
    Route::get( 'get-twitter-response', 'TweetController@getTwitterResponse' );
    Route::get( 'populate-text-from-documents', 'TweetController@populateTextFromDocuments' );
    Route::get( 'inject-and-prepare-data', 'TweetController@injectAndPrepareData' );
    Route::post( 'search', 'TweetController@search' );
    // Route::post( 'send-message', 'TweetController@sendMessage' );
    Route::post( 'retweet', 'TweetController@reTweet' );
    Route::post( 'send-message', 'TweetController@sendDirectMessage' );
    Route::post( 'reply', 'TweetController@reply' );

    Route::get( 'login', 'TwitterAuthController@login' )->name('twitter.login');
    Route::get( 'callback', 'TwitterAuthController@callback' )->name('twitter.callback');
    Route::get( 'logout', 'TwitterAuthController@logout' )->name('twitter.logout');
    Route::get( 'topics', 'TweetController@topics' )->name('twitter.topics');
} );

Route::get('social/authentication', 'HomeController@socialAuthenticationBy');

/**
 * Testing middleware
 */

Route::get( 'view/test', function () {
    return json_encode( auth()->user()->isStudent() );
} );

Route::get( '/view/student', function () {
    return 'only student can view student page';
} )->middleware( 'student' );

Route::get( '/view/admin', function () {
    return 'only admin can view admin page';
} )->middleware( 'admin' );

Route::get( '/view/school', function () {
    return 'only school can view school page';
} )->middleware( 'school' );

Route::get( '/view/tutor', function () {
    return 'only tutor can view tutor page';
} )->middleware( 'tutor' );

Route::get( '/view/counselor', function () {
    return 'only counselor can view counselor page';
} )->middleware( 'counselor' );

Route::get( '/view/parent', function () {
    return 'only parent can view parent page';
} )->middleware( 'parent' );

Auth::routes( ['verify' => true] );

Route::get( '/{vue_route?}', 'HomeController@index' )->where( 'vue_route', '(.*)' )->middleware( 'auth' );




