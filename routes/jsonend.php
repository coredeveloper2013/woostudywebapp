<?php

Route::group( ['prefix' => 'json'], function () {


    /**
     * Profile page
     */

    Route::get( 'public/profile/{userName}', 'ProfileController@publicProfile' );
    Route::get( 'get_auth_user_data', 'ProfileController@get_auth_user_data' );

    //personal info
    Route::get( 'get_profile_personal_info/{profile_id}', 'ProfileController@get_profile_personal_info' );
    Route::post( 'post_profile_personal_info', 'ProfileController@post_profile_personal_info' );


    //contact info
    Route::get( 'get_contact_details/{profile_id}', 'ProfileController@get_contact_details' );
    Route::post( 'post_contact_details', 'ProfileController@post_contact_details' );


    // social media url
    Route::get( 'get_social_media_details/{profile_id}', 'ProfileController@get_social_media_details' );
    Route::post( 'post_social_media_details', 'ProfileController@post_social_media_details' );


    // education details
    Route::get( 'get_education_details/{profile_id}', 'ProfileController@get_education_details' );
    Route::post( 'post_education_details', 'ProfileController@post_education_details' );
    Route::delete( 'delete_education_item/{id}', 'ProfileController@delete_education_item' );
    Route::put( 'update_education_item/{id}', 'ProfileController@update_education_item' );


    // ielts or tofel details
    Route::get( 'get_ielts_details/{profile_id}', 'ProfileController@get_ielts_details' );
    Route::post( 'post_ielts_details', 'ProfileController@post_ielts_details' );
    Route::delete( 'delete_ielts_item/{id}', 'ProfileController@delete_ielts_item' );
    Route::put( 'update_ielts_item/{id}', 'ProfileController@update_ielts_item' );


    //Institute
    Route::resource( 'institute-programs', 'ProgramController' );
    Route::resource( 'institute', 'InstituteController' );
    Route::resource( 'education-level', 'EducationLevelController' );
    Route::resource( 'ielts-exam-level', 'IeltsExamLevelController' );

    //Countries, regions
    Route::get( 'regions', 'RegionController@index' );
    Route::get( 'countries', 'CountryController@index' );
    Route::get( 'states/country/{id}', 'StateController@byCountry' );
    Route::resource( 'states', 'StateController' );
    Route::resource( 'address', 'AddressController' );

    Route::resource( 'department', 'DepartmentController' );
    Route::resource( 'institute-role', 'InstituteRoleController' );

    //Users
    Route::group( ['namespace' => 'Auth'], function () {
        Route::post( 'password-change', 'UserController@changPassword' );
    } );

    Route::group( ['namespace' => 'User', 'prefix' => 'user'], function () {
        // interest
        Route::resource( 'interests', 'UserInterestController' );
        Route::resource( 'educations', 'UserEducationController' );
        Route::resource( 'ielts', 'UserIeltsController' );
        Route::resource( 'videos', 'UserVideoController' );
        Route::resource( 'avater', 'UserImageController' );
        Route::resource( 'experience', 'UserExperienceController' );
        Route::resource( 'licence', 'UserLicenceController' );
        Route::resource( 'course', 'UserCourseController' );
        Route::resource( 'institute-author', 'InstituteAuthorController' );
    } );

    Route::group( ['prefix' => 'chat'], function () {
        Route::post( 'friend/send-message', 'ChattingController@sendMessage' );
        Route::get( 'friend/message/history/{id}', 'ChattingController@history' );
        Route::get( 'friend/list/recent', 'ChattingController@chatFriendLists' );
    } );
    //find connections
    Route::group( ['namespace' => 'Find', 'prefix' => 'finds'], function () {

        //search  the program, course, student
        Route::get( 'institute/programs', 'InstituteController@findPrograms' );
        Route::get( 'tutor/courses', 'TutorController@findCourses' );
        Route::get( 'tutor/courses/student', 'TutorController@findCoursesForStudent' );
        Route::get( 'students', 'StudentController@findStudents' );
        Route::get( 'students/for-student', 'StudentController@findStudentsForStudent' );

        //Send Invitation
        Route::post( 'student/invite', 'StudentController@sendStudentInvitation' );
        Route::post( 'tutor/course/invite', 'TutorController@sendCourseInvitation' );
        Route::post( 'institute/program/invite', 'InstituteController@sendProgramInvitation' );


        //Route::post( 'send/invitation', 'InvitationController@send' );
        Route::get( 'friend/count', 'FriendController@count' );
        Route::get( 'friend/list', 'FriendController@index' );
        Route::get( 'invitation/pending-lists', 'InvitationController@invitationPendingList' );
        Route::get( 'invitation/pending-lists/program', 'InvitationController@invitationPendingProgramList' );
        Route::get( 'invitation/pending-lists/program/first', 'InvitationController@invitationPendingProgramFirst' );
        Route::get( 'invitation/pending-lists/student', 'InvitationController@invitationPendingStudentList' );
        Route::get( 'invitation/pending-lists/student/first', 'InvitationController@invitationPendingStudentFirst' );
        Route::get( 'invitation/pending-lists/course', 'InvitationController@invitationPendingCourseList' );
        Route::get( 'invitation/pending-lists/course/first', 'InvitationController@invitationPendingCourseFirst' );
        Route::patch( 'invitation/accept', 'InvitationController@acceptRequest' );
        Route::patch( 'invitation/accept/all', 'InvitationController@acceptAllRequest' );
        Route::patch( 'invitation/ignore', 'InvitationController@ignoreRequest' );
        Route::patch( 'invitation/ignore/all', 'InvitationController@ignoreAllRequest' );

    } );

    Route::post( 'media/video', 'MediaController@uploadVideo' );
    Route::post( 'posts/video', 'PostController@videoPost' );
    Route::resource( 'posts', 'PostController' )->except( ['create', 'edit'] );
    Route::resource( 'comments', 'CommentController' )->only( ['store', 'update', 'delete'] );
    Route::patch( 'notifications/{id}/read', 'NotificationController@markAsRead' );
    Route::patch( 'notifications/{id}/unread', 'NotificationController@markAsUnRead' );
    Route::get( 'notifications/read', 'NotificationController@readNotification' );
    Route::get( 'notifications/unread', 'NotificationController@unreadNotification' );
    Route::get( 'notifications/count', 'NotificationController@unreadNotificationCount' );
    Route::get( 'notifications', 'NotificationController@index' );

} );


